package start;

import java.util.ArrayList;

public class NumbersIntoWords {

	private static String[] tensNames = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty",
			"ninety" };
	private static String[] onesNames = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
			"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
			"nineteen" };

	public static void main(String arg[]) {

		String digit = "34";
		String words = tens(digit);
		System.out.println(words);

	}

	private static String tens(String digit) {
		String words = "";
		int number = Integer.parseInt(digit);
		if (number < 20) {
			words += onesNames[number];
		} else {
			int tensDigit = number / 10;
			int onesDigit = number % 10;
			words += tensNames[tensDigit];
			if (onesDigit != 0) {
				words += " " + ones(words, onesDigit);
			}
			// System.out.println(words);
		}
		return words;
	}

	private static String ones(String words, int onesDigit) {
		words = onesNames[onesDigit];
		return words;
	}

	public static String wordifyNum(String num) {
		String retValue = "";
		if (num.length() == 1) {

		} else if (num.length() == 2) {

		} else {
			ArrayList<String> arr = splitNum(num);
		}

		return null;

	}

	private static ArrayList<String> splitNum(String num) {
		ArrayList<String> retList = new ArrayList<>();
		retList.add(num.substring(num.length() - 3));
		String s = num.substring(0, num.length() - 3);
		while (s.length() != 0) {
			retList.add(num.substring(num.length() - 2));
		}

		return null;
	}

	public static String lastK(String s, int k) {
		return s.substring(s.length() - k);
	}

	public static String firstK(String s, int k) {
		return s.substring(0, s.length() - k);
	}

}
